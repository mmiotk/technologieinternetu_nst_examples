# TechnologieInternetu_nst_examples

Przykłady do wykładu z przedmiotu Technologie Internetu (ZAO) prowadzone na PJATK Gdańsk

## Przykłady

Każdy z przykładów znajduje się na **osobnej gałęzi** w repozytorium.
Aby wejść do przykładów z poziomu terminala należy skorzystać z polecenia:

```git checkout <nazwa_galezi>```

### Spis przykładów

- Wykład 2 (Zaawansowane elementy w języku znaczników HTML5) - [Link do przykładów](https://gitlab.com/mmiotk/technologieinternetu_nst_examples/-/tree/lecture_2)